﻿using System;
using tessnet2;  
using System.Drawing;  
using System.Drawing.Drawing2D;  
using System.Drawing.Imaging; 

namespace image_reco
{
    class Program
    {
	public const string TesseractDataFolderPath = "~/Document/tessdata";

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
	    Console.ReadKey();

	    var image = new Bitmap("sample-image.jpg");  
	    var ocr = new Tesseract();  
	    ocr.Init(TesseractDataFolderPath, "eng", false);  
	    var result = ocr.DoOCR(image, Rectangle.Empty);  
	    
	    foreach(tessnet2.Word word in result)  
	    {  
	    	Console.WriteLine(word.Text);  
	    }  
        }
    }
}
